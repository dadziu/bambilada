<!DOCTYPE html 
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
	<meta http-equiv="Reply-To" content="piosenki@bambilada.pl" />
	<link rel="Shortcut icon" href="http://bambilada.pl/ikona.ico" />
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<link rel="stylesheet" href="http://bambilada.pl/freelstyle.css" type="text/css">
	<link rel="stylesheet" href="grafika.css" type="text/css">
	<script src="https://apis.google.com/js/platform.js" async defer> {lang: 'pl'} </script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<?php include $_SERVER['SCRIPT_FILENAME'].'.html'; ?>
</head>
<body>
<!-- top - całość -->
<div id="top">

<?php include 'formgrafika.php'; ?>

<div id="GRADIENT_GORA"></div>

<div id="LOGO">
<a href="http://bambilada.pl/"><img src="http://bambilada.pl/dzieci_spiewaja.png" alt="dzieci" border="0" /></a><br/>
</div>

<!-- szablon --> 
<div id="MENU">
<ul>
<li><a href="http://bambilada.pl/">Strona startowa</a>
<li><a href="http://bambilada.pl/piosenkidladzieci/1.php">Piosenki dla dzieci</a>
<li><a href="http://bambilada.pl/piosenkiztekstem/dorotka.php">Piosenki z tekstem</a>
<li><a href="http://bambilada.pl/bajki/wstep.php">Audiobooki</a>
<li><a href="http://bambilada.pl/bajkiyt/maszainiedzwiedz-badzzdrow.php">Bajki</a>
<li><a href="http://bambilada.pl/form.php">Kontakt</a>
</ul>
</div>

<!-- śledzenie google analytics --> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64740897-1', 'auto');
  ga('send', 'pageview');

</script>