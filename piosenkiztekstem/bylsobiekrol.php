<?php include $_SERVER['DOCUMENT_ROOT'].'/head.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/menu_prawe.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/lista_tekstow.php'; ?>

<div id="TRESC">
<h1>By� sobie kr�l</h1>
<iframe src="https://embed.spotify.com/?uri=spotify:track:424qp9iOlX81zbiKLu48CZ" width="300" height="80" frameborder="0" allowtransparency="true"></iframe><br/>
<div id="TEKST_PIOS">
Ju� gwiazdy l�ni�, ju� dzieci �pi�,<br/>
Sen zmorzy� m� laleczk�.<br/>
Wi�c g��wk� z�� i oczka zmru�,<br/>
Opowiem Ci bajeczk�.<br/>
Wi�c g��wk� z�� i oczka zmru�,<br/>
Opowiem Ci bajeczk�.<br/>
<br/>
By� sobie kr�l, by� sobie pa�,<br/>
i by�a te� kr�lewna.<br/>
�yli w�r�d r�, nie znali burz,<br/>
Rzecz najzupe�niej pewna.<br/>
�yli w�r�d r�, nie znali burz,<br/>
Rzecz najzupe�niej pewna.<br/>
<br/>
Kocha� j� kr�l, kocha� j� pa�,<br/>
kochali si� w kr�lewnie.<br/>
I ona te� kocha�a ich,<br/>
kochali si� wzajemnie.<br/>
I ona te� kocha�a ich,<br/>
kochali si� wzajemnie.<br/>
<br/>
Lecz srogi los, okrutna �mier�<br/>
w udziale im przypad�a.<br/>
Kr�la zjad� pies, pazia zjad� kot,<br/>
kr�lewn� myszka zjad�a.<br/>
Kr�la zjad� pies, pazia zjad� kot,<br/>
kr�lewn� myszka zjad�a.<br/>
<br/>
Lecz �eby Ci, nie by�o �al,<br/>
dziecino ukochana.<br/>
Z cukru by� kr�l, z piernika pa�,<br/>
kr�lewna z marcepana.<br/>
Z cukru by� kr�l, z piernika pa�,<br/>
kr�lewna z marcepana.<br/>
<br/>
</div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/foot.php'; ?>