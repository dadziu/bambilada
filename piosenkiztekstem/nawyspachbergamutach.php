<?php include $_SERVER['DOCUMENT_ROOT'].'/head.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/menu_prawe.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/lista_tekstow.php'; ?>

<div id="TRESC">
<h1>Na Wyspach Bergamutach</h1>
<iframe src="https://embed.spotify.com/?uri=spotify:track:4laSVsvGKWi37Jjmz9VcOa" width="300" height="80" frameborder="0" allowtransparency="true"></iframe><br/>
<div id="TEKST_PIOS">
1<br/>
Na wyspach Bergamutach<br/>
Podobno jest kot w butach,<br/>
Widziano tak�e os�a,<br/>
Kt�rego mr�wka nios�a.<br/>
<br/>
Jest kura samograjka<br/>
Znosz�ca z�ote jajka,<br/>
Na d�bach rosn� jab�ka<br/>
W gronostajowych czapkach.<br/>
<br/>
Jest i wieloryb stary,<br/>
Co nosi okulary,<br/>
Uczone s� �ososie<br/>
W pomidorowym sosie.<br/>
<br/>
I tresowane szczury<br/>
Na szczycie szklanej g�ry,<br/>
Jest s�o� z tr�bami dwiema.<br/>
I tylko wysp tych nie ma.<br/>
<br/>
2<br/>
Na wyspach Bergamutach<br/>
Podobno jest kot w butach,<br/>
Widziano tak�e os�a,<br/>
Kt�rego mr�wka nios�a.<br/>
<br/>
Jest kura samograjka<br/>
Znosz�ca z�ote jajka,<br/>
Na d�bach rosn� jab�ka<br/>
W gronostajowych czapkach.<br/>
<br/>
Jest i wieloryb stary,<br/>
Co nosi okulary,<br/>
Uczone s� �ososie<br/>
W pomidorowym sosie.<br/>
<br/>
I tresowane szczury<br/>
Na szczycie szklanej g�ry,<br/>
Jest s�o� z tr�bami dwiema.<br/>
I tylko wysp tych nie ma.<br/>
<br/>
</div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/foot.php'; ?>