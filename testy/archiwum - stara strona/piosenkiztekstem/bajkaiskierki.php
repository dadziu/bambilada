<?php include $_SERVER['DOCUMENT_ROOT'].'/head.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/menu_prawe.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/lista_tekstow.php'; ?>

<div id="TRESC">
<h1>Bajka Iskierki</h1>
<iframe src="https://embed.spotify.com/?uri=spotify:track:2vlnaAf5KFtepdd2W4NG2p" width="300" height="80" frameborder="0" allowtransparency="true"></iframe><br/>
<div id="TEKST_PIOS">
Z popielnika na Wojtusia<br/>
iskiereczka mruga...<br/>
Chod�, opowiem ci bajeczk�,<br/>
bajka b�dzie d�uga.<br/>
<br/>
By�a sobie raz kr�lewna,<br/>
pokocha�a grajka,<br/>
Kr�l wyprawi� im wesele<br/>
i sko�czona bajka.<br/>
<br/>
By�a sobie Baba Jaga<br/>
mia�a chatk� z mas�a,<br/>
a w tej chatce same dziwy!<br/>
Psst... Iskierka zgas�a.<br/>
<br/>
Z popielnika na Wojtusia<br/>
iskiereczka mruga...<br/>
Chod�, opowiem ci bajeczk�,<br/>
bajka b�dzie d�uga.<br/>
<br/>
Ju� ci Wojtu� nie uwierzy,<br/>
iskiereczko ma�a.<br/>
Chwilk� b�y�niesz,<br/>
potem zga�niesz.<br/>
<br/>
Ot i bajka ca�a.<br/>
<br/>
</div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/foot.php'; ?>