<?php include $_SERVER['DOCUMENT_ROOT'].'/head.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/menu_prawe.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/lista_tekstow.php'; ?>

<div id="TRESC">
<h1>Cztery s�onie</h1>
<iframe src="https://embed.spotify.com/?uri=spotify:track:47ZrmxY9F7W4A5C3OIqocL" width="300" height="80" frameborder="0" allowtransparency="true"></iframe><br/>
<div id="TEKST_PIOS">
1<br/>
By�y raz sobie cztery s�onie,<br/>
Ma�e, weso�e, zielone s�onie,<br/>
Ka�dy z kokardk� na ogonie.<br/>
Hej! Cztery s�onie.<br/>
<br/>
I posz�y sobie w daleki �wiat,<br/>
W dalek� drog�, w weso�y �wiat,<br/>
Hej �wieci s�o�ce, wieje wiatr,<br/>
A one id� w �wiat.<br/>
<br/>
Hej!<br/>
Cztery s�onie, zielone s�onie<br/>
Ka�dy kokardk� ma na ogonie,<br/>
Ten pyzaty, ten smarkaty,<br/>
Kochaj� si� jak wariaty.<br/>
<br/>
2<br/>
P�yn� przez morze cztery s�onie,<br/>
Ma�e, weso�e, zielone s�onie,<br/>
Oj! Gwa�tu! Rety! Jeden tonie!<br/>
Smarkaty tonie.<br/>
<br/>
Na pomoc biegn� mu wszystkie wnet,<br/>
Za tr�b� ci�gn� go i za grzbiet,<br/>
I wyci�gn�y z wody go,<br/>
Wi�c strasznie rade s�!<br/>
<br/>
Hej!<br/>
Cztery s�onie, zielone s�onie<br/>
Ka�dy kokardk� ma na ogonie,<br/>
Ten pyzaty, ten smarkaty,<br/>
Kochaj� si� jak wariaty.<br/>
<br/>
3<br/>
Id� przez g�ry cztery s�onie,<br/>
Ma�e, weso�e, zielone s�onie.<br/>
Wtem jeden w przepa�� straszn� leci<br/>
Patrzcie ten trzeci.<br/>
<br/>
Na pomoc biegn� mu wszystkie wnet,<br/>
Za uszy ci�gn� go i za grzbiet<br/>
I wyci�gn�y wreszcie go<br/>
Wi�c strasznie rade s�!<br/>
<br/>
Hej!<br/>
Cztery s�onie, zielone s�onie<br/>
Ka�dy kokardk� ma na ogonie,<br/>
Ten pyzaty, ten smarkaty,<br/>
Kochaj� si� jak wariaty.<br/>
<br/>
4<br/>
A gdy obesz�y ju� �wiat ca�y<br/>
To podskoczy�y i si� za�mia�y,<br/>
Tr�by podnios�y swe do g�ry<br/>
i tr�bi� w chmury.<br/>
<br/>
Hej �atwo obej�� ten ca�y �wiat,<br/>
Gdy obok brata w�druje brat.<br/>
C� im uczyni� mo�e kto,<br/>
Gdy zawsze razem s�.<br/>
<br/>
Hej!<br/>
Cztery s�onie, zielone s�onie<br/>
Ka�dy kokardk� ma na ogonie,<br/>
Ten pyzaty, ten smarkaty,<br/>
Kochaj� si� jak wariaty.<br/>
<br/>
Cztery s�onie, zielone s�onie<br/>
Ka�dy kokardk� ma na ogonie,<br/>
Ten pyzaty, ten smarkaty,<br/>
Kochaj� si� jak wariaty.<br/>
<br/>
</div>
</div>
<?php include $_SERVER['DOCUMENT_ROOT'].'/foot.php'; ?>