<?php include $_SERVER['DOCUMENT_ROOT'].'/head.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/menu_prawe.php';
include $_SERVER['DOCUMENT_ROOT'].'/piosenkiztekstem/lista_tekstow.php'; ?>

<div id="TRESC">
<h1>Ju� gwiazdy l�ni�</h1>
<iframe src="https://embed.spotify.com/?uri=spotify:track:1lmj6zrgT31qiTpK2pwaml" width="300" height="80" frameborder="0" allowtransparency="true"></iframe><br/>
<div id="TEKST_PIOS">
1<br/>
Ju� gwiazdy l�ni�,<br/>
Ju� dzieci �pi�,<br/>
Sen zmo�y� tw� laleczk�,<br/>
Wi�c g��wk� z��,<br/>
I oczka zm�,<br/>
Opowiem Ci bajeczk�.<br/>
<br/>
Wi�c g��wk� z��,<br/>
I oczka zm�,<br/>
Opowiem Ci bajeczk�.<br/>
<br/>
2<br/>
By� sobie kr�l,<br/>
By� sobie pa�,<br/>
I by�a te� kr�lewna,<br/>
�yli w�r�d r�,<br/>
Nie znali burz,<br/>
Rzecz najzupe�niej pewna.<br/>
<br/>
�yli w�r�d r�,<br/>
Nie znali burz,<br/>
Rzecz najzupe�niej pewna.<br/>
<br/>
3<br/>
Kocha� j� kr�l,<br/>
Kocha� j� pa�,<br/>
Kochali j� we dwoje,<br/>
I ona te� kocha�a ich,<br/>
Kochali si� we troje.<br/>
<br/>
I ona te� kocha�a ich,<br/>
Kochali si� we troje.<br/>
<br/>
4<br/>
Lecz straszny los,<br/>
Okrutna �mier�,<br/>
W udziale im przypad�a,<br/>
Kr�la zjad� pies,<br/>
Pazia zjad� kot,<br/>
Kr�lewn� myszka zjad�a.<br/>
<br/>
Kr�la zjad� pies,<br/>
Pazia zjad� kot,<br/>
Kr�lewn� myszka zjad�a.<br/>
<br/>
5<br/>
Lecz �eby Ci,<br/>
Nie by�o �al,<br/>
Dziecino ukochana,<br/>
Z cukru by� kr�l,<br/>
Z piernika pa�,<br/>
Kr�lewna z marcepana.<br/>
<br/>
Z cukru by� kr�l,<br/>
Z piernika pa�,<br/>
Kr�lewna z marcepana.<br/>
<br/>
</div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/foot.php'; ?>