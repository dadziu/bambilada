<!DOCTYPE html 
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
	<meta http-equiv="Reply-To" content="piosenki@bambilada.pl" />
	<link rel="Shortcut icon" href="http://bambilada.pl/ikona.ico" />
	<link rel="stylesheet" href="http://bambilada.pl/freelstyle.css" type="text/css">
	<link rel="stylesheet" href="grafika.css" type="text/css">
	<script src="https://apis.google.com/js/platform.js" async defer> {lang: 'pl'} </script>
	<?php include $_SERVER['SCRIPT_FILENAME'].'.html'; ?>
	<?php include 'meta.html'; ?>
</head>
<body>

<!-- FB Lubie to -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- top - całość -->
<div id="top">

<?php include 'grafika.php'; ?>

<div id="GRADIENT_GORA"></div>

<div id="LOGO">
<a href="http://bambilada.pl/"><img src="http://bambilada.pl/dzieci_spiewaja.png" alt="dzieci" border="0" /></a><br/>
</div>

<!-- szablon --> 
<div id="MENU">
<ul>
<li><a href="http://bambilada.pl/">Strona startowa</a>
<li><a href="http://bambilada.pl/piosenkidladzieci/1.php">Piosenki dla dzieci</a>
<li><a href="http://bambilada.pl/piosenkiztekstem/babciudrogababciu.php">Piosenki z tekstem</a>
<li><a href="http://bambilada.pl/bajki/wstep.php">Audiobooki</a>
<li><a href="http://bambilada.pl/form.php">Kontakt</a>
</ul>
</div>

<div id="LUBIETO">
<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>

<div id="GOOGLEPLUS">
<div class="g-plusone" data-size="medium" data-annotation="inline" data-width="300"></div>
</div>
</div>

<!-- śledzenie google analytics --> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64740897-1', 'auto');
  ga('send', 'pageview');

</script>