<!DOCTYPE html 
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
	<meta http-equiv="Reply-To" content="piosenki@bambilada.pl" />
	<link rel="Shortcut icon" href="http://bambilada.pl/ikona.ico" />
	<link rel="stylesheet" href="http://bambilada.pl/freelstyle.css" type="text/css">
	<link rel="stylesheet" href="grafika.css" type="text/css">
	<script src="https://apis.google.com/js/platform.js" async defer> {lang: 'pl'} </script>
	<?php include $_SERVER['SCRIPT_FILENAME'].'.html'; ?>
</head>
<body>

<!-- FB Lubie to -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- top - ca�o�� -->
<div id="top">

<img id=Z src="sowa.png" alt="sowa" border="0" />

<div id="GRADIENT_GORA"></div>

<div id="LOGO">
<a href="http://bambilada.pl/"><img src="http://bambilada.pl/dzieci_spiewaja.png" alt="dzieci" border="0" /></a><br/>
</div>

<!-- szablon --> 
<div id="MENU">
<ul>
<li><a href="http://bambilada.pl/">Strona startowa</a>
<li><a href="http://bambilada.pl/piosenkidladzieci/1.php">Piosenki dla dzieci</a>
<li><a href="http://bambilada.pl/piosenkiztekstem/babciudrogababciu.php">Piosenki z tekstem</a>
<li><a href="http://bambilada.pl/bajki/wstep.php">Bajki</a>
<li><a href="http://bambilada.pl/form.php">Kontakt</a>
</ul>
</div>

<div id="LUBIETO">
<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>

<div id="GOOGLEPLUS">
<div class="g-plusone" data-size="medium" data-annotation="inline" data-width="300"></div>
</div>
</div>

<!-- �ledzenie google analytics --> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64740897-1', 'auto');
  ga('send', 'pageview');

</script>
<?php include $_SERVER['DOCUMENT_ROOT'].'/bajki/lista_bajek.php'; ?>

<div id="WSTEP_BAJKI">
<h1>Audiobooki</h1><br/>
Nic tak nie rozwija ma�ego dziecka jak czytanie mu bajek. Czytanie stymuluje rozw�j intelektualny, pobudza wyobra�ni�, rozwija umiej�tno�ci spo�eczne, wzbogaca j�zyk. Dziecko, kt�re s�ucha�o bajek w dzieci�stwie, w przysz�o�ci b�dzie charakteryzowa�o si� wi�ksz� elastyczno�ci� i szybko�ci� my�lenia. B�dzie mu si� �atwiej uczy�, a pisanie wypracowa�, b�dzie sprawia�o mu przyjemno��. Bajka czytana jest o wiele lepsza dla rozwoju ma�ego dziecka, ni� bajka ogl�dana w telewizji, gdzie wygl�d postaci, otoczenia jest podany w tak zwanym "gotowcu". Bajka czytana wymaga od dziecka wi�kszego zaanga�owania si� i pobudzenia neuron�w w m�zgu, kt�re w pierwszych latach �ycia dziecka, rozwijaj� si� bardzo intensywnie.<br/>
Wsp�czesny �wiat wymaga od rodzic�w du�ego zaanga�owania w prac�. Wielu ludzi narzeka, �e ma coraz mniej czasu na sp�dzanie go z w�asnym dzieckiem. Z jednej strony pojawia si� du�a potrzeba wspomagania rozwoju dziecka, a z drugiej strony pojawia si� brak czasu. Sposobem na ten problem jest audiobook, czyli nagrane bajki czytane np. przez aktor�w. S� one o wiele lepsze dla dzieci w wieku 2-6 ni� bajki ogl�dane, kt�re charakteryzuj� si� du�� ilo�ci� bod�c�w zupe�nie niepotrzebnych ma�emu dziecku. Nale�y te� pami�ta�, �e dziecko, kt�re os�ucha si� w bajkach od najm�odszych lat, ch�tniej te� si�gnie samo po ksi��k� ju� w okresie szko�y podstawowej. W wieku 6 - 7 lat dziecko powinno stopniowo czyta� ju� bajki samo, co te� jest lepszym rozwi�zaniem ni� ogl�danie telewizji. I naprawd� jest bardzo rozwijaj�ce - psycholog Dorota Biczak.
<br/><br/>
<div id="POGRUBIENIE">
Naprzeciw potrzebom rodzic�w Bambilada.pl prezentuje audiobooki, kt�rymi w awaryjnych sytuacjach mo�na zast�pi� czytaj�cego rodzica. Jest to idealna alternatywa dla bajki w telewizji. Jest tu szeroki wyb�r rozmaitych opowiada� i bajek, kt�re zainteresuj� nie tylko najm�odszych. Zach�camy!
</div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/footform.php'; ?>