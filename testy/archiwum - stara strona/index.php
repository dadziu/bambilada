<?php include $_SERVER['DOCUMENT_ROOT'].'/head.php'; ?>

<div id="TRESC1">
<h1>Witaj!</h1>
<br/>
Wychowanie dziecka to nie lada wyzwanie dla rodzic�w. Dla prawid�owego rozwoju potrzebuje ono wiele urozmaiconych zabaw. Dzieci uwielbiaj� muzyk� oraz bajki. Rodzic nie zawsze ma czas na wyszukiwanie nowych utwor�w, kt�re nie b�d� si� zn�w powtarza�, a co za tym idzie - zanudza� dziecka. 
Strona bambilada powsta�a g��wnie z my�l� o rodzicach, kt�rzy szukaj� dla swoich pociech ciekawych piosenek. Umo�liwia ona przede wszystkim odtwarzanie muzyki oraz �piewanie razem z dzieckiem. 
<br/>
Zapraszamy!<br/>
*je�eli nie znajdziesz nic ciekawego, to nie zniech�caj si�! Bowiem w najbli�szym czasie na stronie b�dzie si� pojawia� coraz wi�cej interesuj�cych propozycji. Zach�camy do cz�stego odwiedzania.
<br/><br/>
Do prawid�owego odtwarzania piosenek wymagane jest posiadanie darmowego odtwarzacza Spotify.<br/>
<a href="https://www.spotify.com/pl/download/windows/" target="_blank".>Mo�esz pobra� go klikaj�c tu.</a>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/footform.php'; ?>